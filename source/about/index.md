---
title: About
date: 2019-09-13 15:32:30
---
![Dice](https://meeplelikeus.b-cdn.net/wp-content/uploads/2018/03/cameringo_20180225_092747.jpg)


* [Syllabus](http://faculty.ndhu.edu.tw/~chtsao/edu/19/i2p/i2p2019.AM__2080AA.pdf)    Navigator: C. Andy Tsao  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1700 @ AE 316
Office Hours:  Mon. 12:10-13:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* TA Office Hours (NEW):
	* Mon:  A408, Tel: 3517. 1700--1800 劉雅涵, 1800--1900 何尚謙 
	* Tue: A414, Tel:35xx. 1500--1900 陳學蒲
	* Thu: A408, Tel: 3517. 1700--1800 劉雅涵, 1800--1900 何尚謙 
	* Thr: A414: 1100-1200. 陳學蒲
* Prerequisites: Calculus
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)

