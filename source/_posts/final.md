---
title: Final Exam
date: 2020-01-06 03:55:10
tags: [exam]
---
 ![Running](https://f4.bcbits.com/img/a0288819336_10.jpg)
### Final Exam
* 日期: 2020 0107 (二)
* 時間: 1310-1440. 
* 地點: 
	* A307: 學號 $\leq$ 410611375 (27人)
	* A316: 學號  $>$ 410611375 (40人)。
* 範圍：上課及習題內容。對照課本章節約為: Sec. 5.1, 5.2, 5.3, 5.5, 5.6, 5.7, 5.8; Chapter 7; Sec. 8.1, 8.2, 8.5, 8.6; Chapter 9; Chapter 10; Chapter 11; Sec. 13.1, 13.2. (注意： Random vector 本課程中以 $(X_1, X_2)'$ 其中 $X_1, X_2$ 為離散隨機變數為範圍；另外 Law of Large Numbers and Central Limit Theorem 不考）
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!


提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！