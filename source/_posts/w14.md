---
title: Week 14. Normal Again and Quantile
date: 2019-12-19 03:59:44
tags: [moment generating function, quantile]
---
![](https://www.talkcomic.com/wp-content/uploads/2018/03/ogp-660x330.jpg)
### Normal Again

Last week we have discussed [several properties of normal random variables](https://chtsao.gitlab.io/i2p2019/#X-and-aX-b). Essentially, those results say

* If $X$ is a normal random variable, then  $aX +b$ is again normal  for any $a\neq 0, b \in \mathcal{R}$ 
* For any normal, say $X \sim N(\mu, \sigma^2)$ then it can be standardized to a standard normal. i.e. 
$$ \frac{X - E(X)}{\sqrt{Var(X)}}= \frac{X - \mu }{\sigma} \sim N(0, 1).$$
* The mgf of $X \sim (\mu, \sigma^2)$ 
$$ M(t)= e^{\mu t + \frac{1}{2}\sigma^2 t^2}.$$

In a sense, we can convert any normal to a standard normal through standardization. Note that, we can convert a standard normal to any normal. Specifically, 
* Let $Z \sim N(0, 1)$ then $X = \sigma Z + \mu \sim N(\mu, \sigma^2)$ for any $\mu \in \mathcal{R}, \sigma >0.$
* Let the mgf of $X$ and $Z$ are $M_{X}(t), M_{Z}(t)$ respectively. Then
	* $$ M_{X}(t) = E(e^{X t})= E(e^{(\sigma Z + \mu)t })= e^{\mu t} M_{Z}(\sigma t).$$
	* $M_{Z}(t)$ can be computed easily (much easier than direct computation of $M_{X}(t)$). $$M_{Z}(t) = e^{\frac{1}{2}t^2}.$$ 
	
### Quantile
Let $X$ be a random variable with cdf $F$. For any $p \in (0, 1)$, we say $q_p$ is a $p 100$%-th percentile or $p-$th quantile if 
$$ F(q_p) = P(X \leq q_p) = p.$$
In particular, we say $m$ is the median of $X$ if $m=q_{0.5}$ of $X$.
