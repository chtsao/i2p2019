---
title: Overview
date: 2019-09-11 07:59:39
tags: [probability interpretation]
---
![Re Zero](https://i1.wp.com/www.mobileyouth.org/wp-content/uploads/re-zero-season-2.jpg?w=1332&ssl=1)
### 起點設定
De Finetti, 寫了兩大巨冊機率論的機率學者, 一言總結他書的要點 
> Probability does not exist.     機率不存在

機率不存在，那他在寫什麼？我們這門課又在學些什麼呢？
其實，他是以文字矛盾來吸引我們的注意。但不只如此，他以很反諷的方式來提示---機率是我們思想的建構，它不是像石頭、飛鳥一樣的具體實際的物件，而是我們腦中所構想的概念。更重要的每個人也可能不盡相同，而時有不一致或矛盾的發生。

#### 我們的內建機率計算
* 可能有錯誤或矛盾，而且可能每個人不同：[Linda problem](https://puzzlewocky.com/fallacies/the-linda-problem/)
* 機率不只一種，且詮釋/解釋有差別：如以下的機率各有不同的意義
	* 我的基礎機率八成(80%)會過
	* 明天有90%機率會出太陽
	* (醫生說)你再不處理的的蛀牙，整個需要拔掉的機率是75%
   cf. [Interpretations of Probability](https://plato.stanford.edu/entries/probability-interpret/)

因此，建立一個不矛盾的架構，或大致上沒有不一致的機率規範 (norm) 或清楚的機率公理 (axioms)，是讓我們了解機率，避免錯誤的一個開始。經過了許多學者世代交替的努力，目前的機率論提供了一個方便而大致上可以操作的架構。

這課是機率的第一門課。簡單總結，就是

$$\Large x \Rightarrow  X.$$

### Class Info
改放在 [About](https://chtsao.gitlab.io/i2p2019/about/) 中
